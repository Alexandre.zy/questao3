package conta;

public class exercicio3 {
	
	public static void main(String[] args) {
		conta programador = new conta();
		ContaCorrente programadorCC = new ContaCorrente();
		ContaPoupanca programadorCP = new ContaPoupanca();
		
		//Recebendo o sal�rio e atualizando a conta
		programador.deposita(5000);
		programadorCC.deposita(5000);
		programadorCP.deposita(5000);
		
		programador.atualiza(0.01);
		programadorCC.atualiza(0.01);
		programadorCP.atualiza(0.01);
		
		//Exibindo informa��es
		System.out.printf("Saldo da Conta Corrente: %.2f\n",programadorCC.getSaldo());
		System.out.printf("Saldo da Poupan�a:  %.2f\n", programadorCP.getSaldo());
		
		//Tirando 5 reais do caf� em cada conta
		programador.saca(5);
		programadorCC.saca(5);
		programadorCP.saca(5);
		
		programador.atualiza(0);
		programadorCC.atualiza(0);
		programadorCP.atualiza(0);
		
		//Exibindo informa��es
		System.out.println("\nDepois do cafezinho: ");
		System.out.printf("Saldo da Conta Corrente: %.2f\n",programadorCC.getSaldo());
		System.out.printf("Saldo da Poupan�a:  %.2f\n", programadorCP.getSaldo());
	}

}
